#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run \
    --name ${CONTAINER_NAME} \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e START_KEYBASE="false" \
    -dt ${CONTAINER_TEST_IMAGE} ash

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'type openssl'
docker exec -t ${CONTAINER_NAME} ash -c 'type packer'

# clean up
docker rm -f ${CONTAINER_NAME}
