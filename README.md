[![pipeline status](https://gitlab.com/open-source-devex/containers/build-packer/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/containers/build-packer/commits/master)

# Docker container to run builds of packer projects

Based on `registry.gitlab.com/open-source-devex/containers/build:latest` and `hashicorp/packer:light` with tooling for 
building projects during CI.

# This container includes support for mounting Keybase volumes

If you want to use this start the container with the environment variable `START_KEYBASE="true"` and set the variables 
`KEYBASE_PAPERKEY` and `KEYBASE_USERNAME`. For more information see the [Keybase container](https://gitlab.com/open-source-devex/containers/keybase)

## Building new containers without code changes

Because every commit to master in this repository creates a release that tagged in git, the best way to trigger a new 
container build without the code having changed is to create an empty commit.
Such builds are needed for example when the base container has been updated.

```bash
git commit --allow-empty -m "Trigger build of new container on latest base image"